# foxupdate

Utility to update Xenia Linux installation.

## Usage

Refer to [wiki](https://wiki.xenialinux.com/en/latest/usage/update.html).
