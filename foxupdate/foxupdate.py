#!/usr/bin/env python3

import foxcommon as fc
import os
import tomllib
import tomli_w
import urllib
import argparse
import subprocess

from datetime import datetime


ARCH_TABLE = {"x86_64": "amd64"}

arch = ARCH_TABLE[
    subprocess.run(["arch"], shell=True, capture_output=True, text=True).stdout.strip()
]


def load_config():
    with open(os.path.join("/etc/foxupdate", "config.toml"), "rb") as config:
        return tomllib.load(config)


def get_flavours(ret):
    manifest = tomllib.load(urllib.request.urlopen(f"{ret['repository']}Manifest.toml"))
    return [flavour for flavour in manifest.keys() if arch in manifest[flavour]["arch"]]


def get_releases(ret):
    manifest = tomllib.load(urllib.request.urlopen(f"{ret['repository']}Manifest.toml"))
    return [
        release
        for release in manifest[ret["flavour"]]["versions"]
        if arch in manifest[ret["flavour"]]["versions"][release]["arch"]
    ]


def get_root_date():
    return datetime.fromtimestamp(os.path.getmtime("/roots/root.img"))


def get_url_date(url):
    return datetime.strptime(
        urllib.request.urlopen(url).info()["Last-Modified"], "%a, %d %b %Y %H:%M:%S %Z"
    )


def get_url(config_parsed):
    manifest = tomllib.load(
        urllib.request.urlopen(f"{config_parsed['repository']}Manifest.toml")
    )

    filename = manifest[config_parsed["flavour"]]["versions"][
        config_parsed["release_branch"]
    ]["filename"]

    return f"{config_parsed['repository']}{arch}/{config_parsed['release_branch']}/{filename}"


def mount() -> None:
    if not os.path.exists("/mnt/root"):
        os.mkdir("/mnt/root")

    fc.info("Mounting root.img on /mnt/root")
    fc.execute("mount -o ro,loop -t squashfs /roots/root.img.new /mnt/root")

    fc.info("Mounting ESP on /mnt/root/boot/efi")
    fc.execute("mount -L EFI /mnt/root/boot/efi")

    fc.info("Mounting special filesystems")
    fc.execute("mount -t proc /proc /mnt/root/proc")
    fc.execute("mount --rbind /dev /mnt/root/dev")
    fc.execute("mount --rbind /sys /mnt/root/sys")
    fc.execute("mount --bind /run /mnt/root/run")
    fc.execute("mount --make-slave /mnt/root/run")


def is_efi() -> bool:
    return os.path.isdir("/sys/firmware/efi")


def update_grub():
    mount()

    device = (
        "/dev/"
        + subprocess.run(
            "lsblk -no pkname `findmnt /boot/efi | awk '$2 ~ /dev/ {print $2}'`",
            shell=True,
            capture_output=True,
            text=True,
        ).stdout.split("\n")[0]
    )

    if is_efi():
        fc.chroot(
            f"""grub-install --modules=lvm --efi-directory="/boot/efi" --boot-directory="/boot/efi"
grub-mkconfig -o /boot/efi/grub/grub.cfg""", "/mnt/root"
        )
    else:
        fc.chroot(
            f"""grub-install --modules=lvm --boot-directory="/boot/efi" {device}
grub-mkconfig -o /boot/efi/grub/grub.cfg""", "/mnt/root"
        )

    fc.chroot("grub-mkconfig -o /boot/efi/grub/grub.cfg", "/mnt/root")


def update(config_parsed):
    try:
        fc.info("Remounting root as rw")
        fc.execute("mount -o rw,remount /roots")

        if os.path.isfile("/roots/roots.img.bak"):
            fc.info("Found old backup image, replacing with current")

        fc.info("Downloading new root")

        url = get_url(config_parsed)

        current_date = get_root_date()
        new_root_date = get_url_date(url)

        if current_date > new_root_date:
            fc.warn(
                "Your current root is newer than the one being downloaded. Press enter to continue, or ctrl+c to exit."
            )
            input()

        urllib.request.urlretrieve(url, "/roots/root.img.new")
        update_grub()

        os.rename("/roots/root.img", "/roots/root.img.bak")
        os.rename("/roots/root.img.new", "/roots/root.img")
    except:  # I know this is horrific
        fc.warn("Update failed. Cleaning up.")

        if os.path.exists("/roots/root.img.new"):
            os.remove("/roots/root.img.new")

        elif os.path.exists("/roots/root.img.bak"):
            if os.path.exists("/roots/root.img"):
                os.remove("/roots/root.img")

            os.rename("/roots/root.img.bak", "/roots/root.img")

        fc.warn("Raising original error.")
        raise

    fc.info("Installed new root")
    fc.info("Your system is now ready to reboot!")


def setup_config(reload=False):
    path = "/etc/foxupdate"
    config_file = {}

    config_exists = False

    if os.path.exists(os.path.join(path, "config.toml")):
        fc.info("Existing config found, loading config.")
        config_file = load_config()
        config_exists = True
    else:
        fc.info("Generating initial config")
        os.makedirs(path, exist_ok=True)

    validity = {
        "repository": {
            "func": fc.check_url,
            "mode": "check",
            "default": (
                config_file["repository"]
                if config_exists
                else "https://repo.xenialinux.com/releases/"
            ),
            "valid_text": "a URL that points to a Xenia repository (remember the trailing slash)",
        },
        "flavour": {
            "func": get_flavours,
            "mode": "execute",
            "default": config_file["flavour"] if config_exists else "gnome-systemd",
            "return": True,
        },
        "release_branch": {
            "func": get_releases,
            "mode": "execute",
            "default": config_file["release_branch"] if config_exists else "unstable",
            "return": True,
        },
    }

    config_parsed = fc.parse_config(
        config_file if not reload else {}, interactive=True, VALIDITY=validity
    )

    with open(os.path.join(path, "config.toml"), "wb") as config:
        tomli_w.dump(config_parsed, config)

    fc.info("Config setup!")

    return config_parsed


def parse_args():
    parser = argparse.ArgumentParser(
        prog="foxupdate", description="Updates a Xenia Linux system."
    )
    parser.add_argument(
        "-c",
        "--config",
        required=False,
        action="store_true",
        help="Re-initialises config.",
    )
    parser.add_argument(
        "-u",
        "--update",
        required=False,
        action="store_true",
        help="Updates the system.",
    )

    args = parser.parse_args()
    return vars(args)


def main():
    if os.geteuid() != 0:
        fc.die("foxupdate requires root privileges to run")

    args = parse_args()

    # if args["config"] == True:
    #     if os.path.isfile("/etc/foxupdate/config.toml"):
    #         os.remove("/etc/foxupdate/config.toml")

    config = setup_config(args["config"])

    if args["update"] == True:
        update(config)
        return

    current_date = get_root_date()
    new_root_date = get_url_date(get_url(config))

    print(
        f"""
\033[1m\033[35mfoxupdate\033[0m
---------

\033[1mRepository:\033[0m {config["repository"]}
\033[1mFlavour:\033[0m {config["flavour"]}
\033[1mRelease branch:\033[0m {config["release_branch"]}
"""
    )

    if new_root_date > current_date:
        print(
            "An update is available! Run \033[1m\033[32mfoxupdate -u\033[0m to update."
        )


if __name__ == "__main__":
    main()
