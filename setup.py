#!/usr/bin/env python3

import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "foxupdate",
    version = "1.0.0",
    author = "Luna Deards",
    author_email = "luna@xenialinux.com",
    description= ("Updater for Xenia."),
    license = "GPL-3",
    keywords = "update utility",
    url = "https://gitlab.com/xenia-group/foxupdate",
    packages = ["foxupdate"],
    requires=["foxcommon", "tomli_w", "tomllib"],
    entry_points={
        'console_scripts': [
            'foxupdate = foxupdate.foxupdate:main',
        ],
    },
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Package"
        "License :: OSI Approved :: GPL-3"
    ],
)
